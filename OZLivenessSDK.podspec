Pod::Spec.new do |s|
  s.name = 'OZLivenessSDK'
  s.version = '7.3.0'
  s.summary = 'OZLivenessSDK'
  s.homepage = 'https://gitlab.com/akAlmir/oz-livenesssdk-test'
  s.authors = { 'oz-forensics' => 'info@ozforensics.ru' }
  s.source = { :git => 'https://gitlab.com/akAlmir/oz-livenesssdk-test' }
  s.ios.deployment_target  = '12.0'
  s.default_subspec = 'Full'
  s.user_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  
  s.subspec 'Full' do |ss|
    ss.vendored_frameworks = 'OZLivenessSDK.xcframework'
    ss.resources = ['OZLivenessSDKResources.bundle', 'OZLivenessSDKOnDeviceResources.bundle']
  end
  
  s.subspec 'Core' do |ss|
    ss.vendored_frameworks = 'OZLivenessSDK.xcframework'
    ss.resources = 'OZLivenessSDKResources.bundle'
  end
  
  s.dependency 'TensorFlowLiteC', '2.11.0'

end

